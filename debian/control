Source: mpdscribble
Section: sound
Priority: optional
Maintainer: mpd maintainers <pkg-mpd-maintainers@lists.alioth.debian.org>
Uploaders: Geoffroy Youri Berret <kaliko@debian.org>
Build-Depends: debhelper-compat (= 13),
               meson,
               pkgconf,
               libboost-dev,
               libcurl4-gnutls-dev | libcurl-dev,
               libgcrypt20-dev,
               libmpdclient-dev (>= 2.10),
               libsystemd-dev [linux-any],
               systemd-dev [linux-any],
               po-debconf
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/mpd-team/mpdscribble.git
Vcs-Browser: https://salsa.debian.org/mpd-team/mpdscribble
Homepage: https://www.musicpd.org/clients/mpdscribble/
Rules-Requires-Root: no

Package: mpdscribble
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends},
    ucf,
    adduser,
Suggests: mpd
Provides: mpd-client
Description: Last.fm reporting client for mpd
 Music Player Daemon client which collects information about played tracks and
 submits them to the Last.fm social music network (formerly known as
 Audioscrobbler). If submission servers are not reachable, submissions are
 enqueued and stored on disk cache.
 .
 The client can be also configured to use other scrobbling services like
 Libre.fm.
 .
 This package contains daemon which can be optionally installed system wide.
